//Priprava knjižnic
var formidable = require("formidable");
var util = require('util');

if (!process.env.PORT)
  process.env.PORT = 8080;

// Priprava povezave na podatkovno bazo
var sqlite3 = require('sqlite3').verbose();
var pb = new sqlite3.Database('chinook.sl3');

// Priprava strežnika
var express = require('express');
var expressSession = require('express-session');
var streznik = express();
streznik.set('view engine', 'ejs');
streznik.use(express.static('public'));
streznik.use(
  expressSession({
    secret: '1234567890QWERTY', // Skrivni ključ za podpisovanje piškotkov
    saveUninitialized: true,    // Novo sejo shranimo
    resave: false,              // Ne zahtevamo ponovnega shranjevanja
    cookie: {
      maxAge: 3600000           // Seja poteče po 60min neaktivnosti
    }
  })
);

var razmerje_usd_eur = 0.877039116;

function davcnaStopnja(izvajalec, zanr) {
  switch (izvajalec) {
    case "Queen": case "Led Zepplin": case "Kiss":
      return 0;
    case "Justin Bieber":
      return 22;
    default:
      break;
  }
  switch (zanr) {
    case "Metal": case "Heavy Metal": case "Easy Listening":
      return 0;
    default:
      return 9.5;
  }
}

// Prikaz seznama pesmi na strani
//var trenutna = zahteva.session.neki;

streznik.get('/', function(zahteva, odgovor) {
  //var trenutna = zahteva.session.neki;
  //if (trenutna != null){
  if (zahteva.session.trenutnaStranka != null){
    pb.all("SELECT Track.TrackId AS id, Track.Name AS pesem, \
            Artist.Name AS izvajalec, Track.UnitPrice * " +
            razmerje_usd_eur + " AS cena, \
            COUNT(InvoiceLine.InvoiceId) AS steviloProdaj, \
            Genre.Name AS zanr \
            FROM Track, Album, Artist, InvoiceLine, Genre \
            WHERE Track.AlbumId = Album.AlbumId AND \
            Artist.ArtistId = Album.ArtistId AND \
            InvoiceLine.TrackId = Track.TrackId AND \
            Track.GenreId = Genre.GenreId \
            GROUP BY Track.TrackId \
            ORDER BY steviloProdaj DESC, pesem ASC \
            LIMIT 100", function(napaka, vrstice) {
      if (napaka)
        odgovor.sendStatus(500);
      else {
          for (var i=0; i<vrstice.length; i++)
            vrstice[i].stopnja = davcnaStopnja(vrstice[i].izvajalec, vrstice[i].zanr);
          odgovor.render('seznam', {seznamPesmi: vrstice});
      }
                                                    })
  }
  else {
    odgovor.redirect('/prijava');
  }
})

// Dodajanje oz. brisanje pesmi iz košarice
streznik.get('/kosarica/:idPesmi', function(zahteva, odgovor) {
  var idPesmi = parseInt(zahteva.params.idPesmi);
  if (!zahteva.session.kosarica)
    zahteva.session.kosarica = [];
  if (zahteva.session.kosarica.indexOf(idPesmi) > -1) {
    zahteva.session.kosarica.splice(zahteva.session.kosarica.indexOf(idPesmi), 1);
  } else {
    zahteva.session.kosarica.push(idPesmi);
  }
  
  odgovor.send(zahteva.session.kosarica);
});

// Vrni podrobnosti pesmi v košarici iz podatkovne baze
var pesmiIzKosarice = function(zahteva, callback) {
  if (!zahteva.session.kosarica || Object.keys(zahteva.session.kosarica).length == 0) {
    callback([]);
  } else {
    pb.all("SELECT Track.TrackId AS stevilkaArtikla, 1 AS kolicina, \
    Track.Name || ' (' || Artist.Name || ')' AS opisArtikla, \
    Track.UnitPrice * " + razmerje_usd_eur + " AS cena, 0 AS popust, \
    Genre.Name AS zanr \
    FROM Track, Album, Artist, Genre \
    WHERE Track.AlbumId = Album.AlbumId AND \
    Artist.ArtistId = Album.ArtistId AND \
    Track.GenreId = Genre.GenreId AND \
    Track.TrackId IN (" + zahteva.session.kosarica.join(",") + ")",
    function(napaka, vrstice) {
      if (napaka) {
        callback(false);
      } else {
        for (var i=0; i<vrstice.length; i++) {
          vrstice[i].stopnja = davcnaStopnja((vrstice[i].opisArtikla.split(' (')[1]).split(')')[0], vrstice[i].zanr);
        }
        callback(vrstice);
      }
    })
  }
}

streznik.get('/kosarica', function(zahteva, odgovor) {
  pesmiIzKosarice(zahteva, function(pesmi) {
    if (!pesmi)
      odgovor.sendStatus(500);
    else
      odgovor.send(pesmi);
  });
})

// Vrni podrobnosti pesmi na računu
var pesmiIzRacuna = function(racunId, callback) {
    pb.all("SELECT Track.TrackId AS stevilkaArtikla, 1 AS kolicina, \
    Track.Name || ' (' || Artist.Name || ')' AS opisArtikla, \
    Track.UnitPrice * " + razmerje_usd_eur + " AS cena, 0 AS popust, \
    Genre.Name AS zanr \
    FROM Track, Album, Artist, Genre \
    WHERE Track.AlbumId = Album.AlbumId AND \
    Artist.ArtistId = Album.ArtistId AND \
    Track.GenreId = Genre.GenreId AND \
    Track.TrackId IN (SELECT InvoiceLine.TrackId FROM InvoiceLine, Invoice \
    WHERE InvoiceLine.InvoiceId = Invoice.InvoiceId AND Invoice.InvoiceId = " + racunId + ")",
    function(napaka, vrstice) {
      //console.log(vrstice);
      if (napaka){
       callback(false); //500
     }
     else {
       for (var i=0; i<vrstice.length; i++) {
          vrstice[i].stopnja = davcnaStopnja((vrstice[i].opisArtikla.split(' (')[1]).split(')')[0], vrstice[i].zanr);
        }

       callback (vrstice);
     }
     //callback(napaka,vrstice);
     
    
    })
}

// Vrni podrobnosti o stranki iz računa
var strankaIzRacuna = function(racunId, callback) {
    pb.all("SELECT Customer.* FROM Customer, Invoice \
            WHERE Customer.CustomerId = Invoice.CustomerId AND Invoice.InvoiceId = " + racunId,
    function(napaka, vrstice) {
      //console.log(vrstice);
      callback(napaka,vrstice);
    })
}

// Izpis računa v HTML predstavitvi na podlagi podatkov iz baze

streznik.post('/izpisiRacunBaza', function(zahteva, odgovor) {
  
  var form = new formidable.IncomingForm();
  form.parse(zahteva, function (napaka1, polja, datoteke) {
    //console.log(polja);
    //console.log(polja.seznamRacunov);
    //console.log(datoteke);
    //var id =
    if (napaka1==true){
      odgovor.send('Napaka1.');
    }
    else {
      strankaIzRacuna(parseInt(polja.seznamRacunov), function(napaka2, kupec){
        if (napaka2==true){
         odgovor.send('Napaka2.');
        }
        else {
          pesmiIzRacuna(parseInt(polja.seznamRacunov), function(pesmi){
            if (pesmi == false){
              odgovor.sendStatus(500);
            }
           else  {
              if (pesmi.length == 0){
                odgovor.send("<p>V košarici nimate nobene pesmi, \
                zato računa ni mogoče pripraviti!</p>");
              }
              else {
               
                odgovor.setHeader('content-type', 'text/xml');
                odgovor.render('eslog', {vizualiziraj: true, kupec: kupec [0], postavkeRacuna: pesmi});
                
                
              }
            }
          })
        }
      })
    }
  
  //odgovor.end();
  })
})


var stranka = function(strankaId, callback) {
  pb.get("SELECT Customer.* FROM Customer WHERE Customer.CustomerId = $cid", {},
    function(napaka, vrstica) {
      callback(false);
  });
}

// Izpis računa v HTML predstavitvi ali izvorni XML obliki
streznik.get('/izpisiRacun/:oblika', function(zahteva, odgovor) {
  pesmiIzKosarice(zahteva, function(pesmi) {
    if (!pesmi) {
      odgovor.sendStatus(500);
    } else if (pesmi.length == 0) {
      odgovor.send("<p>V košarici nimate nobene pesmi, \
        zato računa ni mogoče pripraviti!</p>");
    } else {
      vrniStranke(function (napaka, stranke){
        if (napaka){
          odgovor.send("<p>Prišlo je do napake.</p>");
          odgovor.end();
        }
        if (stranke[zahteva.session.trenStr] == undefined){
          odgovor.send("<p>Stranka ni izbrana.</p>");
        } //dela
        else {
         // try {
          //console.log (stranke[zahteva.session.trenStr]);
          odgovor.setHeader('content-type', 'text/xml');
          odgovor.render('eslog', {vizualiziraj: zahteva.params.oblika == 'html' ? true : false, kupec: stranke[zahteva.session.trenStr], postavkeRacuna: pesmi})
          //}
          /*catch(err){
            odgovor.send("<p>Napaka.</p>");
          }*/ //ne dela
        }
      })
    }
  })
})

// Privzeto izpiši račun v HTML obliki
streznik.get('/izpisiRacun', function(zahteva, odgovor) {
  odgovor.redirect('/izpisiRacun/html')
})

// Vrni stranke iz podatkovne baze
var vrniStranke = function(callback) {
  pb.all("SELECT * FROM Customer",
    function(napaka, vrstice) {
      callback(napaka, vrstice);
    }
  );
}

// Vrni račune iz podatkovne baze
var vrniRacune = function(callback) {
  pb.all("SELECT Customer.FirstName || ' ' || Customer.LastName || ' (' || Invoice.InvoiceId || ') - ' || date(Invoice.InvoiceDate) AS Naziv, \
          Invoice.InvoiceId \
          FROM Customer, Invoice \
          WHERE Customer.CustomerId = Invoice.CustomerId",
    function(napaka, vrstice) {
      callback(napaka, vrstice);
    }
  );
}

// Registracija novega uporabnika
var uspesno = "";
streznik.post('/prijava', function(zahteva, odgovor) {
  var form = new formidable.IncomingForm();
  
  form.parse(zahteva, function (napaka, polja, datoteke) {
    if(polja.FirstName.length > 0 && polja.LastName.length > 0 && polja.Company.length > 0 && polja.Address.length > 0 && polja.City.length > 0 && polja.State.length > 0 && polja.Country.length>0 && polja.PostalCode.length > 0 && polja.Phone.length>0 && polja.Fax.length>0 && polja.Email.length > 0  ){
      pb.run("INSERT INTO Customer (FirstName, LastName, Company, Address, City, State, Country, PostalCode, \
	            Phone, Fax, Email, SupportRepId) VALUES (?,?,?,?,?,?,?,?,?,?,?,3)",  [polja.FirstName, polja.LastName, polja.Company, polja.Address, polja.City, polja.State , polja.Country, polja.PostalCode, polja.Phone, polja.Fax, polja.Email] ,
	            {}, function(napaka) {
	              vrniStranke(function(napaka1, stranke) {
                  vrniRacune(function(napaka2, racuni) {
                    //uspesno = "Stranka je bila uspešno registrirana.";
                  try{
                      uspesno = "Stranka je bila uspešno registrirana."
                  }
                  catch (err){
                       //uspesno = "ni ratalo"; //spremeni!!!
                       uspesno = "Prišlo je do napake pri registraciji nove stranke. Prosim preverite vnešene podatke in poskusite znova.";
                       // odgovor.render('prijava', {sporocilo: uspesno, seznamStrank: stranke, seznamRacunov: racuni}); 
                  }
                    odgovor.render('prijava', {sporocilo: uspesno, seznamStrank: stranke, seznamRacunov: racuni});  
                  }) 
                });
      });
    }
    else {
      uspesno = "Prišlo je do napake pri registraciji nove stranke. Prosim preverite vnešene podatke in poskusite znova.";
       pb.run("INSERT INTO Customer (FirstName, LastName, Company, Address, City, State, Country, PostalCode, \
	            Phone, Fax, Email, SupportRepId) VALUES ()" ,
	             {}, function(napaka) {
	              vrniStranke(function(napaka1, stranke) {
                  vrniRacune(function(napaka2, racuni) {
                    odgovor.render('prijava', {sporocilo:uspesno, seznamStrank : stranke, seznamRacunov: racuni} );
                  })
	              });
	           });
    }
  })
})

// Prikaz strani za prijavo
streznik.get('/prijava', function(zahteva, odgovor) {
  vrniStranke(function(napaka1, stranke) {
      vrniRacune(function(napaka2, racuni) {
        odgovor.render('prijava', {sporocilo: "", seznamStrank: stranke, seznamRacunov: racuni});  
        //uspesno = "";
      }) 
    });
})

// Prikaz nakupovalne košarice za stranko
streznik.post('/stranka', function(zahteva, odgovor) {
  var form = new formidable.IncomingForm();
  
  form.parse(zahteva, function (napaka1, polja, datoteke) {
    if (napaka1 == true){
      odgovor.send("</p>Napaka1.</p>");
      
    }
    else{
      //console.log (polja.seznamStrank);
      //console.log(polja.seznamStrank-1);
      zahteva.session.trenStr = polja.seznamStrank-1;
      zahteva.session.trenutnaStranka = 28;
      zahteva.session.kosarica = null;
      odgovor.redirect('/')
    
    }
    //odgovor.redirect('/')
  });
})

// Odjava stranke
streznik.post('/odjava', function(zahteva, odgovor) {
    zahteva.session.trenutnaStranka = null;
    zahteva.session.kosarica = null;
    //zahteva.session.kosarica.length == 0; ne dela
    zahteva.session.trenStr = undefined;
    odgovor.redirect('/prijava') 
})



streznik.listen(process.env.PORT, function() {
  console.log("Strežnik pognan!");
})


